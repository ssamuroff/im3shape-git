#!/usr/bin/env python
import sys
import os
dirname = os.path.split(__file__)[0]
im3dir  = os.path.abspath(os.path.join(dirname, '..') )
sys.path.append(im3dir)

import py3shape
py3shape.main(sys.argv[1:])

