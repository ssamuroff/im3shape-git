#ifndef _H_I3_QUADRATIC_TEST
#define _H_I3_QUADRATIC_TEST
#include "i3_quadratic_test_definition.h"
#include "i3_data_set.h"


#define QUADRATIC_TEST_TRUE_MEAN1 0.3
#define QUADRATIC_TEST_TRUE_VAR1 (0.02*0.02)

#define QUADRATIC_TEST_TRUE_MEAN2 0.8
#define QUADRATIC_TEST_TRUE_VAR2 (0.01*0.01)

#define QUADRATIC_TEST_TRUE_MEAN3 0.025
#define QUADRATIC_TEST_TRUE_VAR3 (0.004*0.004)

#define QUADRATIC_TEST_TRUE_MEAN4 0.5
#define QUADRATIC_TEST_TRUE_VAR4 (0.1*0.1)


#define QUADRATIC_TEST_TRUE_MEAN5 0.25
#define QUADRATIC_TEST_TRUE_VAR5 (0.02*0.02)

i3_flt i3_quadratic_test_likelihood(i3_image * image, i3_quadratic_test_parameter_set * parameters, i3_data_set * dataset);
void i3_quadratic_test_likelihood_derivative(i3_image * model_image, i3_flt like, i3_data_set* data_set, i3_quadratic_test_parameter_set * x, i3_quadratic_test_parameter_set * xprime);

#endif
