#include "i3_image_fits.h"
#include "i3_load_data.h"  //Want this for the datatype getter.
#define BITS_PER_BYTE 8
#define READWRITE 1

int i3_image_save_fits(i3_image * image, char * filename)
{
	fitsfile * output_file;
	int status=0;
	int number_axes = 2;
	long axis_lengths[number_axes];
	axis_lengths[0]=image->nx;
	axis_lengths[1]=image->ny;
	int bitpix = -((int)sizeof(i3_flt))*BITS_PER_BYTE;
	char overwriting_filename[i3_max_string_length];
	int filename_status = snprintf(overwriting_filename,i3_max_string_length,"!%s",filename);
	if(filename_status==-1){
		I3_WARNING("Tried to save filename but too long.  Filename follows.");
		I3_WARNING(overwriting_filename);
	}
	fits_create_file(&output_file, overwriting_filename, &status);
	fits_create_img(output_file, bitpix, number_axes, axis_lengths, &status);
	
	long first_pixel[number_axes];
	first_pixel[0]=1;
	first_pixel[1]=1;
	
	LONGLONG nelements = image->n;
	int datatype = i3_get_fitsio_flt_dtype();
	
	fits_write_pix(output_file, datatype, first_pixel, nelements,image->data, &status);
	fits_close_file(output_file,&status);
	return status;
}

int i3_fits_image_copy_part_into(fitsfile * f, i3_image * image, int x0, int y0)
{
	/* I know the offset by one looks odd, but this makes it consistent 
	with the i3_image_copy_part function. */
	long first_pixel[2];
	first_pixel[0] = x0 + 1;
	first_pixel[1] = y0 + 1;

	long last_pixel[2];
	last_pixel[0] = x0 + image->nx;
	last_pixel[1] = y0 + image->ny;

	long increment[2];
	increment[0] = 1;
	increment[1] = 1;

	int datatype = i3_get_fitsio_flt_dtype();
	i3_flt null = 0.0;
	int any_null = 0;
	int status = 0;

	fits_read_subset(f, datatype, first_pixel, last_pixel, increment, 
		&null, image->data, &any_null, &status);

	return status;
}

i3_image * i3_fits_image_copy_part(fitsfile * f, int x0, int y0, int width, int height)
{
	i3_image * image = i3_image_create(width,height);
	int fail = i3_fits_image_copy_part_into(f, image, x0, y0);
	if (fail){
		i3_image_destroy(image);
		i3_print(i3_verb_noisy, "Failed to copy part of an image - full image:%ldx%ld sub image: %ldx%ld,  start: %ld, %ld", image->nx, image->ny, width, height, x0, y0);
		return NULL;
	}
	return image;
}
