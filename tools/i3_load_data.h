#ifndef _H_I3_LOAD_DATA
#define _H_I3_LOAD_DATA

#include "i3_image.h"
#include "i3_global.h"

/**
 * i3_read_fits_image
 * Read the image from the primary (0) HDU in the named fits file.
 * This is simply a convenience function to call i3_read_fits_image_extension
 * with the first HDU in the file.
**/
i3_image * i3_read_fits_image(char * filename);



/**
 * i3_read_fits_image_extension
 * Load the image from the numbered HDU in the named fits file.
 * 
 * The returned image is an i3_image type.
**/
i3_image * i3_read_fits_image_extension(char * filename, const int hdu);

/**
 * Get the fitsio data type corresponding to the definition of i3_flt.
**/
int i3_get_fitsio_flt_dtype();

/**
 * Convert the array of floats to i3_flt
**/
int i3_array_float_into_i3_flt(float * src, i3_flt * dst, int n);

/**
 * Convert the array of i3_flt to floats
**/
int i3_array_i3_flt_into_float(i3_flt * src, float * dst, int n);

int i3_fits_cube_number_images(char * filename, int hdu);

/**
\param filename Name of the fits file
\param hdu HDU number to read
\param first_image Index of the first image to read (starts with 0)
\param max_images Number of images to read (pass 0 for all available)
\param number_images Outputs the number of images actually read
*/
i3_image ** i3_read_fits_cube(char * filename, int hdu, int first_image, int max_images, int * number_images);

#endif


