#ifndef _H_I3_GREAT
#define _H_I3_GREAT

#include "i3_global.h"
#include "i3_model_tools.h"
#include "i3_image.h"
#include "i3_psf.h"


#define GREAT08_IMAGES_PER_ROW 100
#define GREAT08_ROWS_PER_FILE 100
#define GREAT08_IMAGES_PER_FILE (GREAT08_IMAGES_PER_ROW*GREAT08_ROWS_PER_FILE)

#define GREAT10_IMAGES_PER_ROW 100
#define GREAT10_ROWS_PER_FILE 100
#define GREAT10_IMAGES_PER_FILE (GREAT10_IMAGES_PER_ROW*GREAT10_ROWS_PER_FILE)
#define GREAT10_IMAGE_SIZE 48


//#warning JAZ MADE THESE NEGATIVE AGAIN. G08 WARNING
// Sarah made g1 positive again, so is same as G08 instructions except g1 is opposite sign
// NB. These values are no longer used by the im3shape driver for GREAT08 - see ucl_des_shear/great/funcpsf_great08_psf000*.txt
#define GREAT08_STAR_SET_1_G1 0.019
#define GREAT08_STAR_SET_1_G2 -0.007
	
#define GREAT08_STAR_SET_2_G1 -0.007
#define GREAT08_STAR_SET_2_G2 -0.019
	
#define GREAT08_STAR_SET_3_G1 0.038
#define GREAT08_STAR_SET_3_G2 -0.014

#define GREAT08_PSF_FWHM_PIXELS 2.85
#define GREAT08_PSF_TRUNCATION_PIXELS 5.7
//#define GREAT08_PSF_TRUNCATION_PIXELS 2.85
//double the psf fwhm for truncation.
//#define GREAT08_PSF_FWHM_PIXELS 0.001 // useful for debugging
//#define MOFFAT_RADIUS_FOR_UNIT_FWHM 1.019649
// 20110617SLB I reckon it should be 1/ the above number
#define MOFFAT_RADIUS_FOR_UNIT_FWHM 0.98072959
#define GREAT08_MOFFAT_BETA 3.0
#define GREAT08_ORIGINAL_STAMP_SIZE 40


i3_fourier * i3_build_great08_kernel(int set, int nx, int ny, int upsampling);

i3_image * i3_great08_psf(int start_set, int npix, i3_flt x0, i3_flt y0, int upsampling);

i3_moffat_psf * i3_great08_psf_form(int start_set, int stamp_size);



i3_moffat_psf * i3_great10_psf_catalog_read(char * filename, int *n);
i3_fourier * i3_build_great10_kernel(i3_moffat_psf * psf,int npix, int upsampling, int padding, int psf_type);

#endif
