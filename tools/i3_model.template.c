#include "i3_model.h"
#include "i3_options.h"
#include "i3_model_tools.h"
#include "string.h"
#include <stdlib.h>
#include <stdio.h>


void i3_model_setup_offsets(i3_model * model){
	model->nbytes = 0;
	model->byte_offsets = malloc(sizeof(int)*model->nparam);
	#python setup_offsets
}

void i3_model_destroy(i3_model * model){
	free(model->param_fixed);
	free(model->byte_offsets);
	free(model->min);
	free(model->max);
	free(model->param_type);
	free(model);
}

i3_model * i3_model_create(char * name, i3_options * options){
	i3_model * model = malloc(sizeof(i3_model));
	strcpy(model->name,name);
	#python model_creation
	else{
		free(model);
		return NULL;
	}
	i3_model_setup_offsets(model);
	model->min = i3_model_option_min(model,options);
	model->max = i3_model_option_max(model,options);
	return model;	
}

i3_parameter_set * i3_model_option_widths(i3_model * model, i3_options * options){
	i3_parameter_set * p = (i3_parameter_set*)malloc(model->nbytes);
	#python widths_code
	return p;
}

void i3_model_extract_ellipticity(i3_model * model, i3_parameter_set * p, i3_flt * e1, i3_flt * e2){
	#python extract_ellipticity
}

bool i3_model_any_nan(i3_model * model, i3_parameter_set * p)
{
	#python any_nan_code
	return false;
}

void i3_model_option_fixes(i3_model * model, i3_options * options){
	#python fixes_code
}

int i3_model_set_parameter_by_name(i3_model * model, i3_parameter_set * p, char * name, double value)
{
	#python parameter_set_by_name_code
	return 1;
}

i3_parameter_set * i3_model_option_starts(i3_model * model, i3_options * options){
	i3_parameter_set * p = (i3_parameter_set*)malloc(model->nbytes);
	#python starts_code
	return p;
}

i3_parameter_set * i3_model_option_min(i3_model * model, i3_options * options){
	i3_parameter_set * p = (i3_parameter_set*)malloc(model->nbytes);
	#python min_code
	return p;
}

i3_parameter_set * i3_model_option_max(i3_model * model, i3_options * options){
	i3_parameter_set * p = (i3_parameter_set*)malloc(model->nbytes);
	#python max_code
	return p;
}

i3_parameter_set * i3_model_option_range(i3_model * model, i3_options * options){
	i3_parameter_set * p = (i3_parameter_set*)malloc(model->nbytes);
	#python range_code
	return p;
}


void i3_model_posterior_derivative(i3_model * model, i3_image * model_image, i3_flt likelihood, i3_data_set * data_set, i3_parameter_set * parameters, i3_parameter_set * derivative){
	if (!model->posterior_derivative) I3_FATAL("Attempted to compute derivative of model, but not available",22);
	model->posterior_derivative(model_image,likelihood,data_set,parameters,derivative);
	
}




int i3_model_number_varied_params(i3_model * model){
	int n=0;
	for(int i=0;i<model->nparam;i++){
		if (!model->param_fixed[i]) n++;
	}
	return n;
}

int i3_model_number_varied_params_nonzero_width(i3_model * model, i3_parameter_set * width){
	int n=0;
	#python nonzero_width_nparam_code
	return n;
}


void i3_model_copy_parameters(i3_model * model, i3_parameter_set * restrict dest, i3_parameter_set * restrict source){
	memcpy(dest,source,model->nbytes);
}

void i3_model_scale_parameters(i3_model * model, i3_parameter_set * p, i3_flt scale){
	#python scale_code
}

void i3_model_perturb(i3_model * model, i3_parameter_set * p, i3_flt scale)
{
	#python perturb_code
}

const char * i3_model_header_line(i3_model * model)
{
	#python header_line_code
	return "";
}


void i3_model_pretty_fprintf_parameters(i3_model * model, FILE * f, i3_parameter_set * p){
	char s[i3_max_string_length];
	i3_model_parameter_pretty_string(model,p,s,i3_max_string_length);
	fprintf(f,"%s",s);
}

void i3_model_fprintf_parameters(i3_model * model, FILE * f, i3_parameter_set * p){
	char s[i3_max_string_length];
	i3_model_parameter_string(model,p,s,i3_max_string_length);
	fprintf(f,"%s",s);
}

void i3_model_pretty_printf_parameters(i3_model * model, i3_parameter_set * p){
	i3_model_pretty_fprintf_parameters(model,stdout,p);
}

void i3_model_printf_parameters(i3_model * model, i3_parameter_set * p){
	i3_model_fprintf_parameters(model,stdout,p);
}


int i3_model_parameter_string(i3_model * model, i3_parameter_set * p, char * parameter_string, int string_length){
#python parameter_string
	return 0;
}
int i3_model_parameter_pretty_string(i3_model * model, i3_parameter_set * p, char * parameter_string, int string_length){
#python pretty_parameter_string
	return 0;
}

void i3_model_map_physical(i3_model * model, i3_parameter_set * input, i3_parameter_set * output, i3_options * options)
{
	i3_model_copy_parameters(model, output, input);
	if (model->map_physical!=NULL) model->map_physical(input, output, options);
}

void i3_model_starting_position(i3_model * model, i3_parameter_set * p, i3_data_set * data_set, i3_options * options){
	if (model->start==NULL) return;
	model->start(p,data_set,options);
}

i3_flt i3_model_posterior(i3_model * model,i3_image * image, i3_parameter_set * p, i3_data_set * data){
	i3_flt prior = i3_model_prior(model,p);
	if (prior==BAD_LIKELIHOOD) return prior;
	i3_flt likelihood=i3_model_likelihood(model, image, p, data);
	if (likelihood==BAD_LIKELIHOOD) return likelihood;
	return prior+likelihood;
}

i3_flt i3_model_likelihood(i3_model * model,i3_image * image, i3_parameter_set * p, i3_data_set * data){
	i3_flt likelihood=model->likelihood(image,p,data);
	return likelihood;
}


i3_flt i3_model_prior(i3_model * model, i3_parameter_set * p){
	i3_flt prior = i3_model_prior_limits(model,p);
	if (prior==BAD_LIKELIHOOD) return prior;  /* Parameters have been excluded because one of them goes outside its range.  No point going further*/
	if (model->prior!=NULL) prior += model->prior(p);
	return prior;
}

i3_flt i3_model_prior_limits(i3_model * model, i3_parameter_set * p){
#python prior_code	
	return 0.0;
}

bool i3_model_prior_violations(i3_model * model, i3_parameter_set * p, FILE * output){
	bool violated=false;
	
	#python prior_violations
	
	return violated;
}

void i3_model_get_image_into(i3_model * model, i3_parameter_set * params, i3_data_set * dataset, i3_image * image_out){
#python get_model_image_string
}

void i3_model_read_params_from_str_into(i3_model * model, char *parameter_strings[], i3_parameter_set * params){
#python read_params_from_str_string
}

void i3_model_posterior_derivative_approximation(i3_model * model, i3_parameter_set * p, i3_image * model_image, i3_data_set * data_set, i3_parameter_set * pprime, i3_flt * like, i3_flt epsilon){
#python posterior_derivative_approx
}

void i3_model_extract_varied_nonzero_width_parameters(i3_model * model, i3_parameter_set * x, i3_parameter_set * width, i3_flt * output){
        int j=0;
        int offset; 
        double value;
        for(int i=0;i<model->nparam;i++){
                if (model->param_fixed[i]) continue;
                if (!model->param_type[i]==i3_parameter_type_flt) I3_FATAL("Tried to use i3_model_extract_varied_nonzero_width_parameters on non-float parameter.",I3_MATH_ERROR);
                offset = model->byte_offsets[i];  /* The memory position of the parameter in the parameter object. */
                if (0.0==(i3_flt)(*((i3_flt*)(((char*)width)+offset)))) continue; /* Check if width is zero */
                value = (i3_flt)(*((i3_flt*)(((char*)x)+offset)));  /* The memory position of the parameter in the parameter object. */
                output[j++]=value;
        }
}

void i3_model_extract_varied_parameters(i3_model * model, i3_parameter_set * x, i3_flt * output){
        int j=0;
        int offset; 
        double value;
        for(int i=0;i<model->nparam;i++){
                if (model->param_fixed[i]) continue;
                offset = model->byte_offsets[i];  /* The memory position of the parameter in the parameter object. */
                if (!model->param_type[i]==i3_parameter_type_flt) I3_FATAL("Tried to use extract_varied_parmameters on non-float parameter.",I3_MATH_ERROR);
                value = (i3_flt)(*((i3_flt*)(((char*)x)+offset)));  /* The memory position of the parameter in the parameter object. */
                output[j++]=value;
        }
}



void i3_model_input_varied_nonzero_width_parameters(i3_model * model, i3_flt * input, i3_parameter_set * x, i3_parameter_set * width, i3_parameter_set * base_x){
        i3_model_copy_parameters(model,x,base_x);
        
        int j=0;
        for(int i=0;i<model->nparam;i++){
                if (model->param_fixed[i]) continue;
                if (!model->param_type[i]==i3_parameter_type_flt) I3_FATAL("Tried to use i3_model_input_varied_nonzero_width_parameters on non-float parameter.",I3_MATH_ERROR);
                int offset = model->byte_offsets[i];
                if (0.0==(i3_flt)(*((i3_flt*)(((char*)width)+offset)))) continue; /* Check if width is zero */
                *((i3_flt*)(((char*)x)+offset)) = input[j++];
        }
        
}

void i3_model_input_varied_parameters(i3_model * model, i3_flt * input, i3_parameter_set * x, i3_parameter_set * base_x){
        i3_model_copy_parameters(model,x,base_x);
        
        int j=0;
        for(int i=0;i<model->nparam;i++){
                if (model->param_fixed[i]) continue;
                if (!model->param_type[i]==i3_parameter_type_flt) I3_FATAL("Tried to use i3_model_input_varied_parameters on non-float parameter.",I3_MATH_ERROR);
                int offset = model->byte_offsets[i];
                *((i3_flt*)(((char*)x)+offset)) = input[j++];
        }
        
}
