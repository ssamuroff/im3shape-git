#include "i3_image.h"
#include "i3_load_data.h"
#include "i3_image_fits.h"
#include "i3_model_tools.h"
#include "i3_psf.h"


int main(int argc, char * argv[]){
	
	char * output_filename1 = argv[1];
	char * output_filename2 = argv[2];

	
	int n_central = 5;
	int n_central_upsampling = 5;
	i3_flt truncation = 1e30;
	int upsampling = 1;
	int padding = 0;
	
	int nx = 39;
	int ny = 39;
	
	i3_image * image = i3_image_create(nx,ny);
	i3_image_zero(image);
	i3_flt gal_x0 = nx/2 + 0.5;
	i3_flt gal_y0 = ny/2 + 0.5;
	i3_flt gal_ab = 20.0;
	i3_flt gal_e1 = 0.3;
	i3_flt gal_e2 = 0.0;
	i3_flt gal_A = 1.0;
	i3_flt gal_index = 1.0;
	
	i3_add_real_space_sersic_truncated_radius_upsample_central(gal_ab,gal_e1,gal_e2,gal_A,gal_x0,gal_y0,gal_index,truncation,n_central,n_central_upsampling,  image);

	printf("i3ut_convolution1: created an image with center %f %f (remember that DS9 iterates from 1)\n",gal_x0,gal_y0);

	int status; 

	status = i3_image_save_fits(image, output_filename1);
	
	printf("i3ut_convolution1: saved unconvolved image with status %d\n",status);


	int psf_type = 0;
	i3_flt psf_beta = 3.;
        i3_flt psf_e1  = 0.; 
        i3_flt psf_e2  = 0.;
        i3_flt psf_fwhm  = 2.85;

	i3_fourier * kernel = i3_fourier_conv_kernel_moffat(image->nx,upsampling, padding, psf_beta, psf_fwhm, psf_e1, psf_e2, truncation, psf_type);
 
       	i3_image_convolve_fourier( image, kernel );
	
	status = i3_image_save_fits(image, output_filename2);

	printf("i3ut_convolution1: saved convolved image with status %d\n",status);
	return status;
}
