import numpy as np
from . import structs
try:
	import pywcs
except ImportError:
	pywcs=None

def check_pywcs():
	if pywcs is None:
		raise ImportError("You need to have pywcs to use multiple exposure transforms")

def image_to_wcs(fits_filename, x, y, ext=0):
	from .pyfits import pyfits
	check_pywcs()
	if isinstance(fits_filename, pywcs.WCS):
		wcs = fits_filename
	else:
		header = pyfits.getheader(fits_filename, ext)
		wcs = pywcs.WCS(header, relax=True)
	delta = 5.0
	X = [x, x+delta, x]
	Y = [y, y, y+delta]
	ra, dec = wcs.wcs_pix2sky(X, Y, 0)
	ra0, dra_dx, dra_dy = ra
	dec0, ddec_dx, ddec_dy = dec
	cosdec = np.cos(np.radians(dec0))

	T = structs.Transform_struct()
	T.ra0 = ra0
	T.dec0 = dec0
	T.cosdec0 = cosdec
	T.x0 = x
	T.y0 = y
	T.A[0][0] = (dra_dx - ra0) * cosdec / delta
	T.A[0][1] = (dra_dy - ra0) * cosdec / delta
	T.A[1][0] = (ddec_dx - dec0) / delta
	T.A[1][1] = (ddec_dy - dec0) / delta
	return T


def wcs_to_image(fits_filename, ra, dec, ext=0):
	from .pyfits import pyfits
	check_pywcs()
	if isinstance(fits_filename, pywcs.WCS):
		wcs = fits_filename
	else:
		header = pyfits.getheader(fits_filename, ext)
		wcs = pywcs.WCS(header, relax=True)
	delta = 0.000375
	RA = [ra, ra+delta, ra]
	DEC = [dec, dec, dec+delta]
	X, Y = wcs.wcs_sky2pix(RA, DEC, 0)
	x0, dx_dra, dx_ddec = X
	y0, dy_dra, dy_ddec = Y
	cosdec = np.cos(np.radians(dec))

	T = structs.Transform_struct()
	T.ra0 = ra
	T.dec0 = dec
	T.cosdec0 = cosdec
	T.x0 = x0
	T.y0 = y0
	T.A[0][0] = (dx_dra - x0) /cosdec / delta
	T.A[0][1] = (dx_ddec - x0) / delta
	T.A[1][0] = (dy_dra - y0) /cosdec / delta
	T.A[1][1] = (dy_ddec - y0) / delta

	return T

def wcs_to_image_at_pixel(fits_filename, x, y, ext=0):
	import pyfits
	check_pywcs()
	header = pyfits.getheader(fits_filename, ext)
	wcs = pywcs.WCS(header, relax=True)
	T = image_to_wcs(wcs, x, y, ext)
	ra = T.ra0
	dec = T.dec0
	transform = wcs_to_image(wcs, ra, dec, ext)
	# print x, transform.x0, y, transform.y0
	# sys.exit(0)
	return transform

