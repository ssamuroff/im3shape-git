import galsim
import numpy as np
#import pylab

def get_snr(image_no_noise,image_noise):
	noise_var=np.mean((image_no_noise.array - image_noise.array)**2)
	return np.sum(image_no_noise.array*image_noise.array/noise_var)/np.sqrt(np.sum(image_no_noise.array*image_no_noise.array/noise_var))

class ring_dithered_gal(object):
	def __init__(self,shear,n_phi):
		return 


class dithered_gal(object):
	def __init__(self,galaxy,psfs,snr,n_exposures,rng,pixel_scale,stamp_size):
		self.snr=snr
		self.stamp_size=stamp_size
		self.pixel_scale=pixel_scale
		self.psfs=psfs
		self.n_exposures=n_exposures
		self.gal_flux=1.e6 #arbitray
		self.gal=galaxy.withFlux(self.gal_flux) #set input galaxy to have this arbitrary flux
		self.rng = rng
		#Get the noise sigma...multiply this by sqrt(n_exposures) for each exposure
		self.get_noise_sigma()
		self.noise_sigma_per_exposure=self.noise_sigma*np.sqrt(self.n_exposures)

	def get_images_and_transforms(self,scale_offset=1.):
		"""
		Generate n_exposures images of the galaxies, with random offsets in x and y directions
		Offsets are uniform random in range scale_offset*(-1,1)
		"""
		images=[]
		#Get x and y offsets, in range (-1,1)*pixel_scale
		x_offsets,y_offsets=(scale_offset*self.pixel_scale*np.random.uniform(-1,1,self.n_exposures),
		                     scale_offset*self.pixel_scale*np.random.uniform(-1,1,self.n_exposures)
		                    )
		for i in range(self.n_exposures):
			image=galsim.ImageF(self.stamp_size,self.stamp_size)
			#shift by relevant offsets
			this_gal=self.gal.shift(dx=x_offsets[i],dy=y_offsets[i])
			final=galsim.Convolve([self.psfs[i],this_gal])
			#Note assuming here that the psf model does have the pixel integration included.
			final.drawImage(image,scale=self.pixel_scale, method='no_pixel')
			image.addNoise(galsim.GaussianNoise(self.rng, sigma=self.noise_sigma_per_exposure))
			images.append(image.array)

		return images,x_offsets,y_offsets



	def get_noise_sigma(self,test=True):

		noise_sigma_0=0.1*self.gal_flux/(self.stamp_size**2)		
		
		#First draw galaxy once to get snr		
		#noiseless
		final=galsim.Convolve([self.psfs[0],self.gal])
		image_no_noise=galsim.ImageF(self.stamp_size,self.stamp_size)
		final.drawImage(image_no_noise,scale=self.pixel_scale)
		#with noise
		image_noise=galsim.ImageF(self.stamp_size,self.stamp_size)
		final.drawImage(image_noise,scale=self.pixel_scale)
		image_noise.addNoise(galsim.GaussianNoise(self.rng, sigma=noise_sigma_0))
		#calculate snr and new noise sigma
		snr_0=get_snr(image_no_noise,image_noise)
		self.noise_sigma=noise_sigma_0/(self.snr/snr_0)
		#and test
		if test:
			image=galsim.ImageF(self.stamp_size,self.stamp_size)
			final.drawImage(image,scale=self.pixel_scale)
			image_noise.addNoise(galsim.GaussianNoise(self.rng, sigma=self.noise_sigma))
			print 'generated galaxy with snr: ',get_snr(image_no_noise,image_noise)
			print 'input snr was ',self.snr




def main():

	galaxy = galsim.Exponential(flux=1., half_light_radius=1.)
	psf = galsim.Moffat(beta=3., fwhm=0.2)
	n_exposures=4
	pixel_scale=0.26
	stamp_size=48
	d=dithered_gal(galaxy,[psf]*n_exposures,0.01,1000,n_exposures,pixel_scale=pixel_scale,stamp_size=stamp_size)

	images,dxs,dys=d.get_images_and_transforms()
	print images
	for image,dx,dy in zip(images,dxs,dys):

		pylab.imshow(image,origin='lower',interpolation='nearest')
		pylab.title('dx=%f, dy=%f\n dx(pix)=%f, dy(pix)=%f\nx(pix)=%f, y(pix)=%f'%(dx,dy,dx/pixel_scale,dy/pixel_scale,stamp_size/2+dx/pixel_scale,stamp_size/2+dy/pixel_scale))
		pylab.show()

if __name__=="__main__":
	main()
