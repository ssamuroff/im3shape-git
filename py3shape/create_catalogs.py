#Run this on a directory created by meds_mpi_3.py to generate catalog files to enable re-runs
import argparse
import glob
import meds
import os
import numpy as np

def quick_read_row_iobjs(filename):
    f = open(filename)
    row_ids = []
    for i,line in enumerate(f.readlines()):
        line=line.strip()
        if i==0:
            index = line.split().index("row_id")
        elif len(line)==0 or line[0]=='#':
            continue
        else:
            row_ids.append(int(line.split()[index]))
    return row_ids


def make_catalog(base_dir, meds_path):
    #base_dir=e.g. r_2/bulge
    meds_name = os.path.split(meds_path)[1]
    tile_dir = os.path.join(base_dir, meds_name)
    outfile = os.path.join(tile_dir,"catalog.txt")
    main_files = glob.glob(tile_dir+"/*.main.txt")
    completed_ids = set()
    for main_file in main_files:
        completed_ids.update(quick_read_row_iobjs(main_file))
    m=meds.MEDS(meds_path)
    iobjs = np.arange(m.size, dtype=np.int64)
    todo = np.array([iobj not in completed_ids for iobj in iobjs], dtype=bool)
    iobjs = iobjs[todo]
    print "{} -> {}  [{}]".format(tile_dir, outfile, len(iobjs))
    np.savetxt(outfile, iobjs, fmt='%d')


parser=argparse.ArgumentParser(description="Generate catalog files so you can resume a meds_mpi_3 run.")
parser.add_argument("directory", help="Top level directory name (should have bulge/disc subdirs, which themeslves have tile name subdirs)")
parser.add_argument("tile_list", help="Text file containing list of tiles")


args = parser.parse_args()
for line in open(args.tile_list):
    meds_path = line.strip()
    make_catalog(args.directory, meds_path)
