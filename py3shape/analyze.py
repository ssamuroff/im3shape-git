from .image import Image
from .options import Options
from .results import Results
from .model import Model
from . import structs
from . import transformer
from . import lib
from . import utils
import ctypes as ct
import numpy


class PosteriorCalculator(object):
    def __init__(self, options, dataset):
        self.options = options
        self.dataset = dataset
        self.model = Model(options)
        self._baseline = self.model.starting_params(self.options)
        self.params = self.model.starting_params(self.options)
        self.nparam = self.model.number_varied_params()
        self.starting_vector = self.model.extract_varied_params(self.params)

    def __call__(self, param_vector):
        self.model.input_varied_params(param_vector, self.params, self._baseline)
        like =  -self.model.likelihood(self.params, self.dataset)
        return like



def analyze(galaxy, psf, options, weight=None, mask=None, ID=None, x=None, y=None, galaxy_function=None, galaxy_function_args=tuple(), starting_params_only=False):
    galaxy = Image(galaxy)
    if psf is None:
        print "Warning: no PSF"
    else:
        psf = Image(psf)
    if mask is not None:
        mask = Image(mask)
    if weight is not None:
        weight = Image(weight)
        #otherwise the None becomes NULL in the call and weight is ignored
    nx, ny = galaxy.nx, galaxy.ny
    u = options.upsampling
    pad = options.padding
    Nx=(nx+pad)*u
    Ny=(ny+pad)*u
    best = Image(nx, ny)

    if psf is not None:
        assert (psf.nx == Nx) and (psf.ny == Ny), (
            "Was expecting PSF of size %dx%d=((%d+%d)*%d)x((%d+%d)*%d), but got %dx%d" % 
                (Nx, Ny, galaxy.nx, pad, u, galaxy.ny, pad, u, psf.nx, psf.ny) )

    if ID is None:
        ID = utils.ID_from_timestamp() 

    # x and y are the entries of the catalog, optional arguments for bookkeeping
    if x is None:
        x = galaxy.nx//2
    if y is None:
        y = galaxy.ny//2

    if galaxy_function:
        galaxy = galaxy_function(galaxy, *galaxy_function_args)

    result = Results(options.model_name)
    #if this were != 0 we would do no minimization/mcmc and just use start params
    starting_params_only_int = (1 if starting_params_only else 0)
    lib.i3_analyze_wrapper(galaxy, psf, weight, mask, best, options, ID, x, y, result, starting_params_only)

    return result, best.array()


def analyze_multiexposure(galaxies, psfs, weights, transforms, options, ID=None, x=None, y=None, starting_params_only = False, bands=None):

    # sanitize input
    if not  ( len(galaxies) == len(psfs) == len(weights) == len(transforms) ) :
        raise I3StructuralError('lenghts of multi exposure lists have to match: len(galaxies)=%d len(psfs)=%d len(weights)=%d len(transforms)=%d' % (len(galaxies),len(psfs),len(weights),len(transforms)))   
    assert bands is None or len(bands)==len(psfs)
    galaxies = [Image(g) for g in galaxies]
    psfs = [Image(p) for p in psfs]
    weights = [Image(w) for w in weights]

    nx = sum(g.nx for g in galaxies)
    ny = max(g.ny for g in galaxies)
    best = Image(nx, ny)

    #Check PSF sizes
    u = options.upsampling
    pad = options.padding
    for (g,p) in zip(galaxies, psfs):
        nx_g = g.nx
        ny_g = g.ny
        Nx_g=(nx_g+pad)*u
        Ny_g=(ny_g+pad)*u
        assert (p.nx == Nx_g) and (p.ny == Ny_g), (
            "Was expecting PSF of size %dx%d=((%d+%d)*%d)x((%d+%d)*%d), but got %dx%d" % 
                (Nx_g, Ny_g, nx_g, pad, u, ny_g, pad, u, p.nx, p.ny) )

    if ID is None:
        ID = utils.ID_from_timestamp() 

    # x and y are the entries of the catalog, optional arguments for bookkeeping
    if x is None:
        x = 0
    if y is None:
        y = 0

    result = Results(options.model_name)
    galaxies_p = utils.double_pointer_from_list(galaxies, structs.Image_struct_p)
    psfs_p = utils.double_pointer_from_list(psfs, structs.Image_struct_p)
    weights_p = utils.double_pointer_from_list(weights, structs.Image_struct_p)
    transforms_p = utils.double_pointer_from_list(transforms, structs.Transform_struct)
    N = len(galaxies)
    starting_params_only_int = (1 if starting_params_only else 0)

    lib.i3_analyze_exposures_wrapper(N, galaxies_p, psfs_p, weights_p, 
        best, transforms_p, bands, options, ID, x, y, result, starting_params_only_int)

    #Convert format back.  The C code may change things (e.g. subtract BGs)
    #and we want to have those changes back for the main code
    galaxies = [g.array() for g in galaxies]
    weights = [w.array() for w in weights]

    return result, best.array(), galaxies, weights


def convert_psf_ellipticities_to_wcs(psf_params, meds, object_id):
    psf_params_extended = []
    # Convert computed PSF ellipticities into wcs
    # loop over exposures!
    stamp_size = meds.get_cat()[object_id]['box_size']
    
    for exposure_id, psf_param in enumerate(psf_params):
        # extract ellipticities from psf_param
        g1image = psf_param[1]
        g2image = psf_param[2]
        # note, that 0th component contains coadd, hence we start with exposure_id==1
        g1sky, g2sky = meds.convert_g_image2sky(object_id, exposure_id+1, stamp_size, g1image, g2image)
        # The next line is for performing a cross-check only,
        # i.e. convert ellipticities to sky and back and see whether they coincide! 
        #g1sky, g2sky = meds.convert_g_sky2image(object_id, exposure_id+1, stamp_size, g1sky, g2sky)
        # append to-sky-converted ellipticities to extended psf_params
        if len(psf_param) == 3:
            psf_params_extended.append([psf_param[0], g1image, g2image, g1sky, g2sky])
        elif len(psf_param) == 6:
            psf_params_extended.append([psf_param[0], g1image, g2image, g1sky, g2sky,
                                        psf_param[3], psf_param[4], psf_param[5]])

    return psf_params_extended


def count_varied_params(opt):
    model_name = opt.model_name
    model = getattr(structs, opt.model_name+"_parameter_set")
    n=0
    for param in model._fields_:
        p = opt[param[0]+"_fixed"]
        #print param[0], p
        if not p:
            n+=1
    return n
