from pylab import *
import pyfits

eps = 10e-12

for prm in arange(7):

    fname_approx = '../bin/jac_approx_%d.fits' % prm
    hdulist = pyfits.open(fname_approx)
    approx = hdulist[0].data
    hdulist.close()

    fname_exact = '../bin/jac_exact_%d.fits' % prm
    hdulist = pyfits.open(fname_exact)
    exact = hdulist[0].data
    hdulist.close()

    figure(1)
    clf()
    subplot(131)
    hold('on')
    imshow(log(abs(approx+eps)),interpolation='nearest')
    #imshow(approx)
    axis('off')
    title('Numerical Jacobian')
    colorbar(shrink=.25,format='%.0e')
    subplot(132)
    hold('on')
    imshow(log(abs(exact+eps)),interpolation='nearest')
    #imshow(exact) 
    axis('off')
    title('Exact Jacobian')
    colorbar(shrink=.25,format='%.0e')
    subplot(133)
    hold('on')
    imshow(approx-exact,interpolation='nearest')
    axis('off')
    title('Difference')
    colorbar(shrink=.25,format='%.02e')
    subplots_adjust(wspace=0.4)
    fname_fig = 'check_jacobian_%d.png' % prm
    savefig(fname_fig, dpi=None, facecolor='w', edgecolor='w',
            orientation='landscape', papertype=None, format=None,
            transparent=False, bbox_inches=None, pad_inches=0.1)
