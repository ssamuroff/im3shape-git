#include "i3_psf.h"
#include "i3_image_fits.h"

int main(){
	int nx = 48;
	int ny = nx;
	i3_flt fwhm = 12;
	i3_flt e1 = 0.;
	i3_flt e2 = 0.;
	i3_flt truncation = 1000.;
	

	i3_flt x = 23.5;
	i3_flt y = 23.5;


	i3_image * airy = i3_image_create(nx, ny);
	i3_image_zero(airy);
        i3_flt rd = i3_pseudo_airy_radius(fwhm);
	i3_image_add_truncated_pseudo_airy(airy, x, y, rd, e1, e2, truncation);
	i3_image_save_text(airy, "p_airy.txt");
	int stat = i3_image_save_fits(airy, "p_airy.fits");
	if (stat) I3_FATAL("Failed to save image",1);
	
}
